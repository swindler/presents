package com.presents;

import com.presents.sweets.models.Present;
import com.presents.sweets.models.Sweet;
import com.presents.sweets.spec.SweetSpec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Searcher implements ISearcher {
    private Map<Sweet, Integer> sweets;
    private Present present;

    Searcher(Present present) {
        this.present = present;
    }

    @Override
    public List<Sweet> findBySpec(SweetSpec spec) {
        sweets = present.getSweets();
        List<Sweet> result = new ArrayList<>();
        if (sweets == null){
            return result;
        }
        for (Map.Entry<Sweet, Integer> entry : sweets.entrySet()) {
            if (entry.getKey().fitsSpecification(spec)) {
                for (int i = 0; i < entry.getValue(); i++) {
                    result.add(entry.getKey());
                }
            }

        }
        return result;
    }

}
