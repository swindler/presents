package com.presents;

import com.presents.sweets.models.Sweet;
import com.presents.sweets.spec.SweetSpec;

import java.util.List;

public interface ISearcher {
    List<Sweet> findBySpec(SweetSpec spec);
}
