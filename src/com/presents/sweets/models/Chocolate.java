package com.presents.sweets.models;

import com.presents.sweets.spec.Material;

public class Chocolate extends Sweet {

    public Chocolate() {
        super.getSpecification().getMaterials().add(Material.CHOCOLATE);
    }

}
