package com.presents.sweets.models;

import com.presents.sweets.spec.Material;

public class ChocolateCandy extends Sweet {

    public ChocolateCandy() {
        super.getSpecification().getMaterials().add(Material.CHOCOLATE);
        super.getSpecification().getMaterials().add(Material.SUGAR);
    }

}
