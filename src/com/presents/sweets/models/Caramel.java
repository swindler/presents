package com.presents.sweets.models;

import com.presents.sweets.spec.Material;

public class Caramel extends Sweet {


    public Caramel() {
        super.getSpecification().getMaterials().add(Material.CARAMEL);
    }

}
