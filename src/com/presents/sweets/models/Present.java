package com.presents.sweets.models;

import java.util.Map;

public class Present {
    private String owner;
    private Map<Sweet,Integer> sweets;

    public Present(String owner, Map<Sweet, Integer> sweets) {
        this.owner = owner;
        this.sweets = sweets;
    }

    public String getOwner() {
        return owner;
    }

    public Map<Sweet, Integer> getSweets() {
        return sweets;
    }

    public Long getWeight(){
        Long result = 0L;
        for (Map.Entry<Sweet, Integer> entry: sweets.entrySet()) {
            result += entry.getKey().getWeight() * entry.getValue();
        }
        return result;
    }
}
