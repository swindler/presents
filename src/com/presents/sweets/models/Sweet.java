package com.presents.sweets.models;

import com.presents.sweets.spec.Material;
import com.presents.sweets.spec.SweetSpec;

public abstract class Sweet {
    private Long weight;
    private final SweetSpec specification = new SweetSpec();



    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public SweetSpec getSpecification() {
        return specification;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sweet sweet = (Sweet) o;

        if (weight != null ? !weight.equals(sweet.weight) : sweet.weight != null) return false;
        return specification != null ? specification.equals(sweet.specification) : sweet.specification == null;
    }

    @Override
    public int hashCode() {
        int result = weight != null ? weight.hashCode() : 0;
        result = 31 * result + (specification != null ? specification.hashCode() : 0);
        return result;
    }

    public boolean fitsSpecification(SweetSpec specification) {
        boolean result;
        if (specification.getWrapper() != null && getSpecification().getWrapper() != null){
            result = specification.getWrapper().equals(getSpecification().getWrapper());
            if (result) {
                return true;
            }
        }
        if (specification.getName() != null && getSpecification().getName() != null) {
            result = specification.getName().equals(getSpecification().getName());
            if (result) {
                return true;
            }
        }


        for (Material material: getSpecification().getMaterials()){
            if( specification.getMaterials() != null && specification.getMaterials().contains(material)){
                return true;
            }
        }
        return false;
    }
}
