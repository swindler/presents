package com.presents.sweets.spec;

public enum Material {
    CHOCOLATE, CARAMEL, FLAVOUR, SUGAR;
}
