package com.presents.sweets.spec;

public enum Wrapper {
    PAPER, FOIL, CARDBOARD;
}
