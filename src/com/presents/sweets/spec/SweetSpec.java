package com.presents.sweets.spec;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SweetSpec {
    private String name;
    private final List<Material> materials = new ArrayList<>();
    private Wrapper wrapper;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Material> getMaterials() {
        return materials;
    }

    public Wrapper getWrapper() {
        return wrapper;
    }

    public void setWrapper(Wrapper wrapper) {
        this.wrapper = wrapper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SweetSpec sweetSpec = (SweetSpec) o;

        if (name != null ? !name.equals(sweetSpec.name) : sweetSpec.name != null) return false;
        if (materials != null ? !materials.equals(sweetSpec.materials) : sweetSpec.materials != null) return false;
        return wrapper == sweetSpec.wrapper;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (materials != null ? materials.hashCode() : 0);
        result = 31 * result + (wrapper != null ? wrapper.hashCode() : 0);
        return result;
    }
}
