package com.presents;

import com.presents.sweets.models.*;
import com.presents.sweets.spec.Material;
import com.presents.sweets.spec.SweetSpec;
import com.presents.sweets.spec.Wrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Presents {

    public static void main(String[] args) {
        Sweet candy = new ChocolateCandy();
        candy.setWeight(5L);
        candy.getSpecification().setName("Chocolate candy");
        candy.getSpecification().getMaterials().add(Material.SUGAR);
        candy.getSpecification().setWrapper(Wrapper.PAPER);

        Sweet chocolate = new Chocolate();
        chocolate.setWeight(5L);
        SweetSpec chocolateSpecification = chocolate.getSpecification();
        chocolateSpecification.setName("Chocolate NESTLE");
        chocolateSpecification.setWrapper(Wrapper.CARDBOARD);

        Caramel caramel = new Caramel();
        caramel.setWeight(1L);
        caramel.getSpecification().setWrapper(Wrapper.FOIL);
        caramel.getSpecification().setName("Caramel Bee");

        Caramel caramel2 = new Caramel();
        caramel2.setWeight(1L);
        caramel2.getSpecification().setWrapper(Wrapper.FOIL);
        caramel2.getSpecification().setName("Caramel2 Bee");

        Map<Sweet, Integer> content = new HashMap<>();
        content.put(candy, 5);
        content.put(chocolate,2);
        content.put(chocolate,2);
        content.put(caramel, 1);
        content.put(caramel2, 1);

        Present present = new Present("Owner", content);


        System.out.println();

        ISearcher searcher = new Searcher(present);
        SweetSpec spec = new SweetSpec();
        spec.getMaterials().add(Material.CARAMEL);
        spec.setWrapper(Wrapper.CARDBOARD);
        //spec.setName("Chocolate candy");
        //spec.setWrapper(Wrapper.FOIL);
        //spec.setName("Chocolate candy");
        List<Sweet> sweetList = searcher.findBySpec(spec);
        for (Sweet sweet: sweetList) {
            System.out.println(sweet.getSpecification().getName());
        }

        System.out.println("Total present weight: " + present.getWeight());
    }
}
